import { Product } from './../product.model';
import { ProductsService } from './../products.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})

export class CreateProductComponent implements OnInit {
  enteredName = "";
  enteredDescription = "";
  enteredPrice = "";
  enteredQuantity = "";

  product: Product;
  isLoading = false;
  form: FormGroup;
  private mode = "create";
  private productId: string;

  constructor(
    public productService: ProductsService,
    public route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
       name: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      description: new FormControl(null, { validators: [Validators.required] }),
      price: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1)]
      }),
      quantity: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1)]
      }),
    });
    
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("productId")) {
        this.mode = "edit";
        this.productId = paramMap.get("productId");
        this.isLoading = true;
        this.productService.getProduct(this.productId).subscribe(productData => {
          this.isLoading = false;
          this.product = {
            id: productData._id,
            name: productData.name,
            description: productData.description,
            price: productData.price,
            quantity: productData.quantity
          };
          this.form.setValue({
            name: this.product.name,
            description: this.product.description,
            price: this.product.price,
            quantity: this.product.quantity
          });
        });
      } else {
        this.mode = "create";
        this.productId = null;
      }
    });
  }
 
  onSaveProduct() {
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === "create") {
      this.productService.addProduct(
        this.form.value.name,
        this.form.value.description,
        this.form.value.price,
        this.form.value.quantity
      );
    } else { 
      this.productService.updateProduct(
        this.productId,
        this.form.value.name,
        this.form.value.description,
        this.form.value.price,
        this.form.value.quantity
      );
    }
    this.form.reset();
  }

}
