import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Product } from './product.model';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  private products: Product[] = [];

  productData = {
    name: '',
    description: '',
    price: '',
    quantity: '',
  };

  private productsUpdated = new Subject<{
    products: Product[];
    productCount: number;
  }>();

  constructor(
    private http: HttpClient,
    private router: Router
  ) {}

  getProducts(productsPerPage: number, currentPage: number) {
    const queryParams = `?pagesize=${productsPerPage}&page=${currentPage}`;
    this.http
      .get<{ message: string; products: any; maxProducts: number }>(
        'http://localhost:3000/api/products' + queryParams
      )
      .pipe(
        map((productData) => {
          return {
            products: productData.products.map((product) => {
              return {
                name: product.name,
                description: product.description,
                id: product._id,
                price: product.price,
                quantity: product.quantity,
              };
            }),
            maxProducts: productData.maxProducts,
          };
        })
      )
      .subscribe((transformedProductData) => {
        this.products = transformedProductData.products;
        this.productsUpdated.next({
          products: [...this.products],
          productCount: transformedProductData.maxProducts,
        });
      });
  }

  getProductUpdateListener() {
    return this.productsUpdated.asObservable();
  }

  getProduct(id: string) {
    return this.http.get<{
      _id: string;
      name: string;
      description: string;
      price: number;
      quantity: number;
    }>('http://localhost:3000/api/products/' + id);
  }

  addProduct(name: string, description: string, price: any, quantity: any) {
    this.productData.name = name;
    this.productData.description = description;
    this.productData.price = price;
    this.productData.quantity = quantity;
 
    this.http
      .post<{ message: string; product: Product }>(
        'http://localhost:3000/api/products',
        this.productData
      )
      .subscribe(
        (responseData) => {
          this.router.navigate(['/']);
        },
        (err) => {
          console.log('error =', err);
        }
      );
  }

  updateProduct(
    id: any,
    name: any,
    description: any,
    price: any,
    quantity: any
  ) {
    let productData: Product | FormData;

    productData = {
      id: id,
      name: name,
      description: description,
      price: price,
      quantity: quantity,
    };

    this.http
      .put('http://localhost:3000/api/products/' + id, productData)
      .subscribe((response) => {
        console.log('updated product=', response);
        this.router.navigate(['/']);
      });
  }

  deleteProduct(productId: string) {
    return this.http.delete('http://localhost:3000/api/products/' + productId);
  }
}
