const express = require("express");

const Product = require("../models/product");
const checkAuth = require("../middleware/check-auth");

const router = express.Router();

router.post("", checkAuth, (req, res, next) => { 
  console.log('req.body.description', req.body)
  const product = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    quantity: req.body.quantity,
  }); 
  
  product.save().then((createdProduct) => { 
    res.status(201).json({
      message: "Product added successfully",
      // product: createdProduct
      product: {
        ...createdProduct,
        id: createdProduct._id,
      },
    });
  }).catch(err => {
    console.log('err in create product=', err)
  })
});

router.put("/:id", checkAuth, (req, res, next) => {
  if (req.file) {
    const url = req.protocol + "://" + req.get("host");
  }
  const product = new Product({
    _id: req.body.id,
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    quantity: req.body.quantity,
  });
  console.log(product);
  Product.updateOne({ _id: req.params.id }, product).then((result) => {
    res.status(200).json({ message: "Product updated successfully!" });
  });
});

router.get("", (req, res, next) => {
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  const productQuery = Product.find();
  let fetchedProducts;
  if (pageSize && currentPage) {
    productQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }
  productQuery
    .then((documents) => {
      fetchedProducts = documents;
      return Product.count();
    })
    .then((count) => {
      res.status(200).json({
        message: "Products fetched successfully!",
        products: fetchedProducts,
        maxProducts: count,
      });
    });
});

router.get("/:id", (req, res, next) => {
  Product.findById(req.params.id).then((product) => {
    if (product) {
      res.status(200).json(product);
    } else {
      res.status(404).json({ message: "Product not found!" });
    }
  });
});

router.delete("/:id", checkAuth, (req, res, next) => {
  Product.deleteOne({ _id: req.params.id }).then((result) => {
    console.log(result);
    res.status(200).json({ message: "Product deleted successfully!" });
  });
});

module.exports = router;
